﻿package ru.surgacheva.homework;

public class Main extends Animale {
    public static void main(String[] args) {
        encapsulation();
        polymorphism ();
        inheritance();
    }
    //11
    public  static void encapsulation (){
        //1. поля Weight_kg и Weight_gr являются закрытыми в классе Animale
        //2. так как геттеров эти поля не имеют, то изменить их значения в этом классе не получится
        //3. Мы сможем только вывести их значения в этом классе
        
        Animale animale = new Animale();
        animale.setName("Корова");
        animale.setAge(32);
        System.out.print(animale.getName() + " " +  animale.getAge() + " Вес в кг: " + animale.getWeight_kg() + " Вес в гр: " + animale.getWeight_gr() + "\n");
        
        animale.setName("Лошадь");
        animale.setAge(15);
        System.out.print(animale.getName() + " " +  animale.getAge() + " Вес в кг: " + animale.getWeight_kg() + " Вес в гр: " + animale.getWeight_gr() + "\n");
    }
    
    public static void polymorphism(){
       
        //В классе Methods есть два метода Sum с разными параметрами. 
        //При вызове метода, приложение само определяет какой метод ему использовать  (зависит от передаваемых параметров)
        
        Animale animale = new Animale();
        animale.setName("Корова");
        animale.setAge(32);
        
        int Sum = Sum(animale.getWeight_kg(), animale.getWeight_gr());
        System.out.print(animale.getName() + " " +  animale.getAge() + " " + Sum + "\n");
        
        animale.setName("Лошадь");
        animale.setAge(15);
        Sum = Sum("5", "6");
        System.out.print(animale.getName() + " " +  animale.getAge() + " " + Sum + "\n");
    }
    
       public static void inheritance(){
           //1. Главный класс наследуется от класса Animale, который в свою очередь наследуется
           //от класса Methods
           //2. при наследовании от класса Animale мы все так же не можем изменять значения
           //полей  с типом private
           //3. так же при наследовании нам становятся доступными методы класса Methods
          
           int Sum1 = Sum(7, 9);
           Output(String.valueOf(Sum1));
           int Sum2 = Sum("5", "6");
           Output(String.valueOf(Sum2));
       }
}
